; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (sodium stream)
  #:use-module (sodium internal)
  #:use-module (system foreign)
  #:use-module (rnrs base)
  #:use-module (rnrs bytevectors)
  #:export (;; crypto-stream-keybytes
            ;; crypto-stream-noncebytes
            ;; crypto-stream
            ;; crypto-stream-xor

            ;; crypto-stream-keygen

            crypto-stream-chacha20-keybytes
            crypto-stream-chacha20-noncebytes
            crypto-stream-chacha20-xor

            crypto-stream-chacha20-ietf-keybytes
            crypto-stream-chacha20-ietf-noncebytes
            crypto-stream-chacha20-ietf
            crypto-stream-chacha20-ietf-xor
            crypto-stream-chacha20-ietf-xor-ic))

;; XSalsa20 (https://doc.libsodium.org/advanced/stream_ciphers/xsalsa20)

(define crypto-stream-keybytes
  (libsodium-constant "crypto_stream_keybytes"))

(define crypto-stream-noncebytes
  (libsodium-constant "crypto_stream_noncebytes"))

(define crypto-stream
  (let ((proc (libsodium-func "crypto_stream"
                              (list
                               ;; char *c, unsigned long long clen
                               '* unsigned-long
                               ;; char *n, char *k
                               '* '*))))


    (lambda (clen nonce key)

      (assert (=  (bytevector-length nonce)
                  (crypto-stream-noncebytes)))

      (assert (= (bytevector-length key)
                 (crypto-stream-keybytes)))

      (let ((c (make-bytevector clen)))
        (proc
         (bytevector->pointer c) clen
         (bytevector->pointer nonce)
         (bytevector->pointer key))

        ;; return c
        c))))


(define crypto-stream-xor
  (let ((proc (libsodium-func "crypto_stream_xor"
                              (list
                               ;; char *c, char *m
                               '* '*
                               ;; unsigned long long mlen
                               unsigned-long
                               ;; char *n, char *k
                               '* '*))))

    (lambda (m nonce key)

      (assert (=  (bytevector-length nonce)
                  (crypto-stream-noncebytes)))

      (assert (= (bytevector-length key)
                 (crypto-stream-keybytes)))

      (let ((c (make-bytevector (bytevector-length m))))

        (proc
         (bytevector->pointer c)
         (bytevector->pointer m)
         (bytevector-length m)
         (bytevector->pointer nonce)
         (bytevector->pointer key))

        ;; return c
        c))))


(define crypto-stream-keygen
  (let ((proc (libsodium-func "crypto_stream_keygen"
                              (list '*))))
    (lambda ()
      (let ((c (make-bytevector (crypto-stream-keybytes))))
        (proc (bytevector->pointer c))
        c))))

;; ChaCha20 (https://doc.libsodium.org/advanced/stream_ciphers/chacha20)

(define crypto-stream-chacha20-keybytes
  (libsodium-constant "crypto_stream_chacha20_keybytes"))

(define crypto-stream-chacha20-noncebytes
  (libsodium-constant "crypto_stream_chacha20_noncebytes"))

(define crypto-stream-chacha20-xor
  (let ((proc (libsodium-func "crypto_stream_chacha20_xor"
                              (list
                               ;; char *c, char *m
                               '* '*
                               ;; unsigned long long mlen
                               unsigned-long
                               ;; char *n, char *k
                               '* '*))))

    (lambda* (#:key message nonce key)

      (assert (=  (bytevector-length nonce)
                  (crypto-stream-chacha20-noncebytes)))

      (assert (= (bytevector-length key)
                 (crypto-stream-chacha20-keybytes)))

      (let ((c (make-bytevector (bytevector-length message))))

        (proc
         (bytevector->pointer c)
         (bytevector->pointer message)
         (bytevector-length message)
         (bytevector->pointer nonce)
         (bytevector->pointer key))

        ;; return c
        c))))


;; ChaCha20 IETF variant

(define crypto-stream-chacha20-ietf-keybytes
  (libsodium-constant "crypto_stream_chacha20_ietf_keybytes"))

(define crypto-stream-chacha20-ietf-noncebytes
  (libsodium-constant "crypto_stream_chacha20_ietf_noncebytes"))

(define crypto-stream-chacha20-ietf
  (let ((proc (libsodium-func "crypto_stream_chacha20_ietf"
                              (list
                               ;; char *c, unsigned long long clen
                               '* unsigned-long
                               ;; char *n, char *k
                               '* '*))))

    (lambda* (#:key clen nonce key)

      (assert (=  (bytevector-length nonce)
                  (crypto-stream-chacha20-ietf-noncebytes)))

      (assert (= (bytevector-length key)
                 (crypto-stream-chacha20-ietf-keybytes)))

      (let ((c (make-bytevector clen)))
        (proc
         (bytevector->pointer c) clen
         (bytevector->pointer nonce)
         (bytevector->pointer key))

        ;; return c
        c))))

(define crypto-stream-chacha20-ietf-xor
  (let ((proc (libsodium-func "crypto_stream_chacha20_ietf_xor"
                              (list
                               ;; char *c, char *m
                               '* '*
                               ;; unsigned long long mlen
                               unsigned-long
                               ;; char *n, char *k
                               '* '*))))

    (lambda* (#:key message nonce key)

      (assert (=  (bytevector-length nonce)
                  (crypto-stream-chacha20-ietf-noncebytes)))

      (assert (= (bytevector-length key)
                 (crypto-stream-chacha20-ietf-keybytes)))

      (let ((c (make-bytevector (bytevector-length message))))

        (proc
         (bytevector->pointer c)
         (bytevector->pointer message)
         (bytevector-length message)
         (bytevector->pointer nonce)
         (bytevector->pointer key))

        ;; return c
        c))))

(define crypto-stream-chacha20-ietf-xor-ic
  (let ((proc (libsodium-func "crypto_stream_chacha20_ietf_xor_ic"
                              (list
                               ;; char *c, char *m
                               '* '*
                               ;; unsigned long long mlen
                               unsigned-long
                               ;; char *n,
                               '*
                               ;; uint32_t ic
                               uint32
                               ;; char *k
                               '*))))

    (lambda* (#:key message nonce ic key)

      (assert (=  (bytevector-length nonce)
                  (crypto-stream-chacha20-ietf-noncebytes)))

      (assert (= (bytevector-length key)
                 (crypto-stream-chacha20-ietf-keybytes)))

      (let ((c (make-bytevector (bytevector-length message))))

        (proc
         (bytevector->pointer c)
         (bytevector->pointer message)
         (bytevector-length message)
         (bytevector->pointer nonce)
         ic
         (bytevector->pointer key))

        ;; return c
        c))))
