; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (sodium base64)
  #:use-module (sodium internal)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:export (sodium-bin2base64
            sodium-base642bin))

;; TODO this API needs some love. It is currently not exposed.

(define sodium-base64-variant-original 1)
(define sodium-base64-variant-original-no-padding 3)
(define sodium-base64-variant-urlsafe 5)
(define sodium-base64-variant-urlsafe-no-padding 7)

(define sodium-base64-encoded-len
  (let ((proc (pointer->procedure int
                                  (dynamic-func "sodium_base64_encoded_len"
                                                libsodium)
                                  (list size_t int))))

    (lambda (size variant)
      (proc size variant))))

(define sodium-bin2base64
  (let ((proc (pointer->procedure '*
                                  (dynamic-func "sodium_bin2base64"
                                                libsodium)
                                  (list
                                   ;; char * const b64, const size_t b64_maxlen
                                   '* size_t
                                   ;; const unsigned char * const bin, const size_t bin_len
                                   '* size_t
                                   ;; const int variant
                                   int))))

    (lambda* (bv #:key (variant sodium-base64-variant-original))
      (let* ((out (make-bytevector
                   (sodium-base64-encoded-len
                    (bytevector-length bv) variant))))


        (pointer->string (proc
                          (bytevector->pointer out) (bytevector-length out)
                          (bytevector->pointer bv) (bytevector-length bv)
                          variant))))))

(define sodium-base642bin
  (let ((proc (libsodium-func "sodium_base642bin"
                              (list
                               ;; unsigned char * const bin, const size_t bin_maxlen
                               '* size_t
                               ;; const char * const b64, const size_t b64_len
                               '* size_t
                               ;; const char * const ignore, size_t * const bin_len
                               '* '*
                               ;; const char ** const b64_end, const int variant
                               '* int))))

    (lambda* (b64 #:key (variant sodium-base64-variant-original))
      (let ((out (make-bytevector (* 2 (string-length b64))))
            (bin-len-ptr
             (bytevector->pointer (make-bytevector (sizeof '*)))))
        (proc
         (bytevector->pointer out) (bytevector-length out)
         (string->pointer b64) (string-length b64)
         %null-pointer bin-len-ptr
         %null-pointer variant)

        (u8-list->bytevector
         (take (bytevector->u8-list out)
               (pointer-address (dereference-pointer bin-len-ptr))))))))

