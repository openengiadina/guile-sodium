; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (sodium internal)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:export (libsodium
            libsodium-constant
            libsodium-func

            &libsodium-error
            libsodium-error?))

;; Internal helpers

(define libsodium (dynamic-link "@LIBSODIUM_LIBDIR@/libsodium"))

(define-syntax-rule (libsodium-constant name)
  (pointer->procedure
   size_t
   (dynamic-func name libsodium)
   (list)))

(define (libsodium-func name)
  (dynamic-func name libsodium))

(define-condition-type &libsodium-error
  &error
  libsodium-error?
  (function-name libsodium-error-function-name)
  (return-value libsodium-error-return-value))

(define (libsodium-func name arg-types)
  (let ((proc (pointer->procedure int
                                  (dynamic-func name libsodium)
                                  arg-types)))
    (lambda* (#:rest args)
      (let ((return-value (apply proc args)))
        (unless (= 0 return-value)
          (raise
           (condition (&libsodium-error
                       (function-name name)
                       (return-value return-value)))))
        return-value))))

(define (sodium-init)
  (let ((proc (pointer->procedure int
                                  (dynamic-func "sodium_init" libsodium)
                                  '())))
    (when (= -1 (proc))
      (raise
       (condition (&libsodium-error
                   (function-name "sodium_init")))))))

;; libsodium needs to be initialized before any other functions can be called.
;; Initialize now.
(sodium-init)
