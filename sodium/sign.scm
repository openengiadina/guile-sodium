; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (sodium sign)
  #:use-module (sodium internal)
  #:use-module (system foreign)
  #:use-module (rnrs base)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 receive)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:export (crypto-sign-publickeybytes
            crypto-sign-secretkeybytes
            crypto-sign-bytes

            crypto-sign-keypair
            crypto-sign-seed-keypair

            crypto-sign-ed25519-sk-to-seed
            crypto-sign-ed25519-sk-to-pk

            crypto-sign-detatched
            crypto-sign-verify-detatched))

(define crypto-sign-publickeybytes
  (libsodium-constant "crypto_sign_publickeybytes"))

(define crypto-sign-secretkeybytes
  (libsodium-constant "crypto_sign_secretkeybytes"))

(define crypto-sign-bytes
  (libsodium-constant "crypto_sign_bytes"))

(define crypto-sign-seedbytes
  (libsodium-constant "crypto_sign_seedbytes"))

(define crypto-sign-keypair
  (let ((proc (libsodium-func "crypto_sign_keypair"
                              (list
                               ;; unsigned char *pk
                               '*
                               ;; unsigned char *sk
                               '*))))
    (lambda ()
      (let ((pk (make-bytevector (crypto-sign-publickeybytes)))
            (sk (make-bytevector (crypto-sign-secretkeybytes))))
        (proc
         (bytevector->pointer pk)
         (bytevector->pointer sk))

        ;; return pk and sk
        (values pk sk)))))

(define crypto-sign-seed-keypair
  (let ((proc (libsodium-func "crypto_sign_seed_keypair"
                              (list
                               ;; unsigned char *pk
                               '*
                               ;; unsigned char *sk
                               '*
                               ;; const unsigned char *seed
                               '*))))
    (lambda (seed)

      ;; assert correct length of seed
      (assert (= (bytevector-length seed)
                 (crypto-sign-seedbytes)))

      (let ((pk (make-bytevector (crypto-sign-publickeybytes)))
            (sk (make-bytevector (crypto-sign-secretkeybytes))))
        (proc
         (bytevector->pointer pk)
         (bytevector->pointer sk)
         (bytevector->pointer seed))

        ;; return pk and sk
        (values pk sk)))))

(define crypto-sign-ed25519-sk-to-seed
  (let ((proc (libsodium-func "crypto_sign_ed25519_sk_to_seed"
                              (list
                               ;; unsigned char *seed
                               '*
                               ;; const unsigned char *sk
                               '*))))
    (lambda (secret-key)

      ;; assert correct length of secret key
      (assert (= (bytevector-length secret-key)
                 (crypto-sign-secretkeybytes)))

      (let ((seed (make-bytevector (crypto-sign-seedbytes))))
        (proc
         (bytevector->pointer seed)
         (bytevector->pointer secret-key))

        ;; return the seed
        seed))))

(define crypto-sign-ed25519-sk-to-pk
  (let ((proc (libsodium-func "crypto_sign_ed25519_sk_to_pk"
                              (list
                               ;; unsigned char *pk
                               '*
                               ;; const unsigned char *sk
                               '*))))
    (lambda (secret-key)

      ;; assert correct length of secret key
      (assert (= (bytevector-length secret-key)
                 (crypto-sign-secretkeybytes)))

      (let ((pk (make-bytevector (crypto-sign-publickeybytes))))
        (proc
         (bytevector->pointer pk)
         (bytevector->pointer secret-key))

        ;; return the public key
        pk))))

(define crypto-sign-detatched
  (let ((proc (libsodium-func "crypto_sign_detached"
                              (list
                               ;; unsigned char *sig, unsigned long long *siglen_p
                               '* '*
                               ;; const unsigned char *m, unsigned long long mlen
                               '* unsigned-long
                               ;; const unsigned char *sk
                               '*))))
    (lambda* (#:key secret-key message)

      ;; assert correct length of secret-key
      (assert (= (bytevector-length secret-key)
                 (crypto-sign-secretkeybytes)))

      (let ((sig (make-bytevector (crypto-sign-bytes)))
            (siglen-ptr (bytevector->pointer
                         (make-bytevector (sizeof '*)))))
        (proc
         (bytevector->pointer sig) siglen-ptr
         (bytevector->pointer message) (bytevector-length message)
         (bytevector->pointer secret-key))

        ;; return signature
        sig))))

(define crypto-sign-verify-detatched
  (let ((proc (pointer->procedure int
                                  (dynamic-func "crypto_sign_verify_detached"
                                                libsodium)
                                  (list
                                   ;; const unsigned char *sig
                                   '*
                                   ;; const unsigned char *m, unsigned long long mlen
                                   '* unsigned-long
                                   ;; const unsigned char *pk
                                   '*))))

    (lambda* (#:key signature message public-key)

      ;; assert correct length of publick-key
      (assert (= (bytevector-length public-key)
                 (crypto-sign-publickeybytes)))

      ;; assert correct lenght of signature
      (assert (= (bytevector-length signature)
                 (crypto-sign-bytes)))

      (case (proc (bytevector->pointer signature)
                  (bytevector->pointer message) (bytevector-length message)
                  (bytevector->pointer public-key))
        ((0) #t)
        ((-1) #f)
        (else => (lambda (return-value)
                   (raise
                    (condition (&libsodium-error
                                (function-name  "crypto_sign_verify_detatched")
                                (return-value return-value))))))))))

