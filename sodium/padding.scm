; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (sodium padding)
  #:use-module (sodium internal)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:export (sodium-pad
            sodium-unpad))

(define sodium-pad
  (let ((proc (libsodium-func "sodium_pad"
                              (list
                               ;; size_t *padded_bufflen_p
                               '*
                               ;; unsigned char *buf
                               '*
                               ;; size_t unpadded_bufflen
                               size_t
                               ;; size_t blocksize
                               size_t
                               ;; size_t max_buflen
                               size_t))))

    (lambda* (input #:key block-size)

      (let* ((input-length (bytevector-length input))
             ;; size of padded bytevector is at most size of input plus block size
             (max-buflen (+ input-length block-size))
             ;; create a large-enough buffer
             (buffer (make-bytevector max-buflen))
             ;; create a pointer for the returned size
             (padded-bufflen-p (bytevector->pointer
                                (make-bytevector (sizeof '*) 0))))

        ;; copy input bytevector to buffer
        (bytevector-copy! input 0
                          buffer 0
                          input-length)

        ;; call the padding function
        (proc padded-bufflen-p
              (bytevector->pointer buffer)
              input-length
              block-size
              max-buflen)
        ;; return padded bytevector
        (pointer->bytevector
         ;; get pointer of buffer
         (bytevector->pointer buffer)
         ;; take padded-bufflen bytes
         (pointer-address
          (dereference-pointer padded-bufflen-p)))))))

(define sodium-unpad
  (let ((proc (libsodium-func "sodium_unpad"
                              (list
                               ;; size_t *unpadded_bufflen_p
                               '*
                               ;; char *buf
                               '*
                               ;; size_t padded_buflen
                               size_t
                               ;; size_t blocksize
                               size_t))))

    (lambda* (input #:key block-size)

      (let ( ;; create a pointer to the returned unpadded size
            (unpadded-bufflen-p (bytevector->pointer
                                 (make-bytevector (sizeof '*) 0))))


        (proc unpadded-bufflen-p
              (bytevector->pointer input)
              (bytevector-length input)
              block-size)

        ;; return unpadded bytevector
        (pointer->bytevector
         ;; pointer to original padded bytevector
         (bytevector->pointer input)
         ;; only take unpadded_bufflen bytes
         (pointer-address
          (dereference-pointer unpadded-bufflen-p)))))))
