; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (sodium generichash)
  #:use-module (sodium internal)
  #:use-module (system foreign)
  #:use-module (ice-9 format)
  #:use-module (rnrs bytevectors)
  #:export (crypto-generichash-bytes
            crypto-generichash-bytes-min
            crypto-generichash-bytes-max
            crypto-generichash-keybytes
            crypto-generichash-keybytes-min
            crypto-generichash-keybytes-max
            crypto-generichash

            crypto-generichash-state?
            crypto-generichash-init
            crypto-generichash-update
            crypto-generichash-final))

(define crypto-generichash-bytes
  (libsodium-constant "crypto_generichash_bytes"))

(define crypto-generichash-bytes-min
  (libsodium-constant "crypto_generichash_bytes_min"))

(define crypto-generichash-bytes-max
  (libsodium-constant "crypto_generichash_bytes_max"))

(define crypto-generichash-keybytes
  (libsodium-constant "crypto_generichash_keybytes"))

(define crypto-generichash-keybytes-min
  (libsodium-constant "crypto_generichash_keybytes_min"))

(define crypto-generichash-keybytes-max
  (libsodium-constant "crypto_generichash_keybytes_max"))

(define crypto-generichash
  (let ((proc (libsodium-func "crypto_generichash"
                              (list
                               ;; char *out, size_t outlen
                               '* size_t
                               ;; char *in, size_t inlen
                               '* size_t
                               ;; char *key, size_t keylen
                               '* size_t))))
    (lambda* (in #:key key (out-len (crypto-generichash-bytes)))

      (let* ((out (make-bytevector out-len))

             (key-len (if key (bytevector-length key) 0))
             (key-ptr (if key
                          (bytevector->pointer key)
                          %null-pointer)))

        (proc (bytevector->pointer out) out-len
              (bytevector->pointer in) (bytevector-length in)
              key-ptr key-len)

        ;; return the hash code
        out))))

(define-wrapped-pointer-type <crypto-generichash-state>
  crypto-generichash-state?
  wrap-generichash-state unwrap-generichash-state
  (lambda (s p)
    (format p "#<crypto_generichash_state ~x>"
            (pointer-address (unwrap-generichash-state s)))))

(define crypto-generichash-init
  (let ((proc (libsodium-func "crypto_generichash_init"
                              (list
                               ;; crypto_generichash_state *state
                               '*
                               ;; char *key, size_t keylen
                               '* size_t
                               ;; size_t outlen
                               size_t))))
    (lambda* (#:key key (out-len (crypto-generichash-bytes)))

      (let* (;; allocate a pointer
             (state-ptr
              (bytevector->pointer (make-bytevector (sizeof '*) 0)))

             (key-len (if key (bytevector-length key) 0))
             (key-ptr (if key
                          (bytevector->pointer key)
                          %null-pointer)))

        (proc state-ptr
              key-ptr key-len
              out-len)

        ;; wrap the pointer as crypto-generichash-state
        (wrap-generichash-state state-ptr)))))


(define crypto-generichash-update
  (let ((proc (libsodium-func "crypto_generichash_update"
                              (list
                               ;; crypto_generichash_state *state
                               '*
                               ;; char *in, size_t inlen
                               '* size_t))))
    (lambda (state in)
      (proc (unwrap-generichash-state state)
            (bytevector->pointer in) (bytevector-length in))

      ;; return the state (should not have changed)
      state)))

(define crypto-generichash-final
  (let ((proc (libsodium-func "crypto_generichash_final"
                              (list
                               ;; crypto_generichash_state *state
                               '*
                               ;; char *out, size_t outlen
                               '* size_t))))
    (lambda* (state #:key (out-len (crypto-generichash-bytes)))

      (let ((out (make-bytevector out-len)))

        (proc
         (unwrap-generichash-state state)
         (bytevector->pointer out) out-len)

        ;; return the hash code
        out))))
