; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (sodium key-derivation)
  #:use-module (sodium internal)
  #:use-module (sodium stream)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:export (crypto-kdf-bytes-min
            crypto-kdf-bytes-max
            crypto-kdf-contextbytes
            crypto-kdf-keybytes

            crypto-kdf-derive-from-key))

(define crypto-kdf-bytes-min
  (libsodium-constant "crypto_kdf_bytes_min"))

(define crypto-kdf-bytes-max
  (libsodium-constant "crypto_kdf_bytes_max"))

(define crypto-kdf-contextbytes
 (libsodium-constant "crypto_kdf_contextbytes"))

(define crypto-kdf-keybytes
 (libsodium-constant "crypto_kdf_keybytes"))

(define crypto-kdf-derive-from-key
  (let
      ((proc (libsodium-func "crypto_kdf_derive_from_key"
                             (list
                              ;; char *subkey
                              '*
                              ;; size_t subkey_len
                              size_t
                              ;; uint64_t subkey_id
                              uint64
                              ;; char *ctx
                              '*
                              ;; char *key
                              '*))))

    (lambda* (key subkey-id ctx #:key (subkey-length (crypto-kdf-keybytes)))
      ;; TODO this could really use some checks on arguments (e.g. size of context, size of key, etc.)
      (let ((subkey (make-bytevector subkey-length 0)))
        (proc
         (bytevector->pointer subkey)
         subkey-length
         subkey-id
         (bytevector->pointer ctx)
         (bytevector->pointer key))

        ;; return subkey
        subkey))))
