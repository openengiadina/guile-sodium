; SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (sodium hashing short-input)
  #:use-module (sodium internal)
  #:use-module (system foreign)
  #:use-module (rnrs base)
  #:use-module (rnrs bytevectors)
  #:export (crypto-shorthash-bytes
            crypto-shorthash-keybytes
            crypto-shorthash-keygen
            crypto-shorthash))

(define crypto-shorthash-bytes
  (libsodium-constant "crypto_shorthash_bytes"))

(define crypto-shorthash-keybytes
  (libsodium-constant "crypto_shorthash_keybytes"))

(define crypto-shorthash-keygen
  (let ((proc (libsodium-func "crypto_shorthash_keygen"
                              (list
                               ;; const unsigned char *k
                               '*))))
    (lambda ()
      (let ((key (make-bytevector 16)))
        (proc (bytevector->pointer key))
        key))))

(define crypto-shorthash
  (let ((proc (libsodium-func "crypto_shorthash"
                              (list
                               ;; unsigned char *out
                               '*
                               ;; const unsigned char *in,
                               '*
                               ;; unsigned long long inlen
                               size_t
                               ;; const unsigned char *k
                               '*))))

    (lambda* (in #:key key)

      ;; assert key has proper length
      (assert (= (bytevector-length key) 16))

      (let ((out (make-bytevector 8)))
        (proc (bytevector->pointer out)
              (bytevector->pointer in)
              (bytevector-length in)
              (bytevector->pointer key))
        out))))

