-*- mode: org; coding: utf-8; -*-

#+TITLE: Hacking guile-sodium

* Contributing

By far the easiest way to hack on guile-sodium is to develop using Guix:

#+BEGIN_SRC bash
  # Obtain the source code
  cd /path/to/source-code
  guix environment -l guix.scm
  # In the new shell, run:
  autoreconf -vif && ./configure && make check
#+END_SRC

You can now hack this project's files to your heart's content, whilst
testing them from your `guix environment' shell.

To try out any scripts in the project you can now use

#+BEGIN_SRC bash
  ./pre-inst-env scripts/${script-name}
#+END_SRC

** Manual Installation

If you do not yet use  Guix, you will have to install this project's
dependencies manually:
  - autoconf
  - automake
  - pkg-config
  - texinfo
  - guile-hall
  - libsodium

Once those dependencies are installed you can run:

#+BEGIN_SRC bash
autoreconf -vif && ./configure && make check
#+END_SRC
