(hall-description
  (name "sodium")
  (prefix "guile")
  (version "0.1.0")
  (author "pukkamustard")
  (copyright (2020))
  (synopsis "Guile bindings to libsodium.)")
  (description
    "This package provides bindings to libsodium which provides core cryptogrpahic primitives needed to build higher-level tools.")
  (home-page
    "https://inqlab.net/git/guile-sodium.git")
  (license gpl3+)
  (dependencies `())
  (files (libraries
           ((directory
              "sodium"
              ((scheme-file "padding")
               (scheme-file "internal")
               (scheme-file "generichash")
               (scheme-file "key-derivation")
               (scheme-file "stream")
               (scheme-file "sign")
               (directory "hashing"
                          ((scheme-file "short-input")))))))
         (tests ((directory "tests"
                            ((directory "sodium"
                                        ((scheme-file "sign")))))))
         (programs ())
         (documentation
           ((org-file "README")
            (symlink "README" "README.org")
            (text-file "HACKING")
            (directory "doc" ((texi-file "sodium")))))
         (infrastructure
           ((directory ".reuse" ((text-file "dep5")))
            (directory "LICENSES" ((text-file "CC0-1.0.txt")
                                   (text-file "CC-BY-SA-4.0.txt")
                                   (text-file "GPL-3.0-or-later.txt")))
            (scheme-file "guix")
            (scheme-file "hall")))))
