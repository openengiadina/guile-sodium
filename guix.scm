(use-modules
 (guix packages)
 ((guix licenses) #:prefix license:)
 (guix download)
 (guix build-system gnu)
 (gnu packages)
 (gnu packages autotools)
 (gnu packages crypto)
 (gnu packages guile)
 (gnu packages guile-xyz)
 (gnu packages pkg-config)
 (gnu packages texinfo))

(package
 (name "guile-sodium")
 (version "0.1.0")
 (source "./guile-sodium-0.1.tar.gz")
 (build-system gnu-build-system)
 (arguments `())
 (native-inputs
  `(("autoconf" ,autoconf)
    ("automake" ,automake)
    ("pkg-config" ,pkg-config)
    ("texinfo" ,texinfo)
    ("guile-hall" ,guile-hall)))
 (inputs `(("guile" ,guile-3.0)))
 (propagated-inputs `(("libsodium" ,libsodium)))
 (synopsis "Guile bindings to the libsodium cryptographic library.")
 (description
  "This package provides Guile bindings to the libsodium cryptographic library
which provides core cryptographic primitives needed to build higher-level
tools.")
 (home-page "https://inqlab.net/git/guile-sodium.git")
 (license license:gpl3+))
