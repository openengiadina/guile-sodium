(define-module (tests sodium sign)
  #:use-module (sodium sign)

  #:use-module (rnrs bytevectors)

  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-64))


(test-begin "Detached mode")

(let*-values
    (((public-key secret-key) (crypto-sign-keypair))
     ((message) (string->utf8 "Hello world!"))
     ((signature) (crypto-sign-detatched #:secret-key secret-key #:message message)))

  (test-assert "can verify signature"
    (crypto-sign-verify-detatched #:signature signature
                                  #:message message
                                  #:public-key public-key))
 
  (test-assert "wrong message fails verification"
    (not (crypto-sign-verify-detatched #:signature signature
                                       #:message (string->utf8 "Wrong message")
                                       #:public-key public-key)))

  (test-assert "invalid signature fails verification"
    (not (crypto-sign-verify-detatched #:signature (make-bytevector 64)
                                       #:message message
                                       #:public-key public-key))))

(test-end "Detached mode")

(test-begin "Secret Key <-> Seed")

(let*-values
    (((seed) (make-bytevector 32))
     ((public-key secret-key) (crypto-sign-seed-keypair seed)))

  (test-assert "extract seed from secret-key"
      (bytevector=? seed
                    (crypto-sign-ed25519-sk-to-seed secret-key)))

  (test-assert "extract public-key from secret-key"
    (bytevector=? public-key
                  (crypto-sign-ed25519-sk-to-pk secret-key))))

(test-end "Secret Key <-> Seed")
